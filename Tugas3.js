class animal(){
    constructor(nama, tgl_lhir, lapar, mood, energy){
        this.nama = nama;
        this.tgl_lhir = tgl_lhir;
        this.lapar= lapar;   
        this.mood = mood;
        this.energy = energy;
    }
    _makanan = 0;
    getage(){
        return this.calculage();
    }
    calculage(){
        return Date.Now - this.tgl_lhir;
    }

    doplay(){
        energy = energy--;
        lapar = lapar++;
        mood= mood++;
        console.log("jump and run!")
    }
    doeat(){
        lapar = lapar--;
        energy = energy++;
        console.log("makan selesai")
    }
    dosleep(){
        energy = energy++;
        console.log("Nyenyak banget!")
    }
    updatemakanan(){
        this.makanan++;
    }
    

}
class dog extends animal(){
    constructor(nama, tgl_lahir, islapar){
        super(nama);
        super(tgl_lahir);
        super(islapar);
        
    }

    dohow(){
        console.log("garuk");
    }
}
class bird extends animal(){
    constructor(nama, tgl_lahir, islapar){
        super(nama);
        super(tgl_lahir);
        super(islapar);

    }
    dofly(){
        console.log("fly!");

    }

}

const fluffy = new dog("fluffy", "11/11/11",true, 10, 8, 9);
fluffy.dohow();
fluffy.doeat(); 
fluffy.dosleep();

const beo = new bird("beo", "12/12/12", false, 9, 8,10);
beo.doeat();
beo.doplay();
beo.dofly();
